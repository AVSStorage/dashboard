<?php

namespace App\Controller;

use App\Form\ResetPasswordRequestFormType;
use App\Repository\UserRepository;
use App\Security\EmailVerifier;
use App\Serializer\Normalizer\FormNormalizer;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\HttpFoundation\JsonResponse;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ResetPasswordController extends AbstractController
{
    private $serializer;
    private $form;
    public function __construct(FormFactoryInterface $formFactory)
    {
        $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();
        $this->serializer = new Serializer([$normalizer], [$encoder]);
        $this->form = $formFactory->create(ResetPasswordRequestFormType::class);

    }

    public function generateRandomPassword() {
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        $max = strlen($characters) - 1;
        for ($i = 0; $i < 10; $i++) {
            $string .= $characters[mt_rand(0, $max)];
        }

        return $string;
    }

    /**
     * @Route("/reset-password", name="reset_get_password", methods={"GET"},  condition="request.isXmlHttpRequest()")
     */
    public function index()
    {
        $view = $this->form->createView();
        $fields = $view->children;
        $normalizers = new FormNormalizer(new ObjectNormalizer());
        $data = $normalizers->normalize($this->form,'json', ['neededFieldsArray' => [ 'id','full_name', 'label',  'label_attr' , 'required', 'attr'], 'fields' => $fields]);
        $view = $this->serializer->serialize( array_merge($data, ['form_id' => $view->vars['id']]), 'json');
        return JsonResponse::fromJsonString($view);
    }

    /**
     * @Route("/reset-password", name="reset_post_password", methods={"POST"},  condition="request.isXmlHttpRequest()")
     */
    public function reset(Request $request, UserPasswordEncoderInterface $passwordEncoder, MailerInterface $mailer, UserRepository $userRepository) {

        $this->form->handleRequest($request);

        if ($this->form->isSubmitted() && $this->form->isValid()){
            $user = $userRepository->findOneBy(['email' => $this->form->get('email')->getData()]);
            if ($user) {
                $password = $this->generateRandomPassword();
                // encode the plain password
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $password
                    )
                );

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $email = (new TemplatedEmail())
                    ->from(new Address('bh41574@piter3.dns-rus.net', 'Algroo.com Платформа объявлений'))
                    ->to($user->getEmail())
                    ->subject('Восстановление пароля')
                    ->htmlTemplate('reset_password/email.html.twig')
                    ->context([
                        'password' => $password
                    ]);

                try {
                    $mailer->send($email);
                } catch (TransportException $error) {

                }

                return JsonResponse::fromJsonString($this->serializer->serialize( ['message' => 'Данные были обновлены. Проверьте Вашу почту.'], 'json'));
            } else {
                return new JsonResponse(['message' => 'Такого пользователя не существует'], Response::HTTP_NOT_ACCEPTABLE);
            }
        }

        return new JsonResponse(['message' => $this->form->getErrors()->current()->getMessage()], Response::HTTP_NOT_ACCEPTABLE);
    }
}
