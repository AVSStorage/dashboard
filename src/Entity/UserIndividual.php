<?php

namespace App\Entity;

use App\Repository\UserIndividualRepository;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserIndividualRepository::class)
 *
 */
class UserIndividual
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Поле является обязательным")
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="user_individual", cascade={"persist", "remove"})
     */
    private $user_link;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserLink(): ?User
    {
        return $this->user_link;
    }

    public function setUserLink(?User $user_link): self
    {
        $this->user_link = $user_link;

        // set (or unset) the owning side of the relation if necessary
        $newUser_individual = null === $user_link ? null : $this;
        if ($user_link->getUserIndividual() !== $newUser_individual) {
            $user_link->setUserIndividual($newUser_individual);
        }

        return $this;
    }
}
