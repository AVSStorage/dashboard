<?php


namespace App\Event;


use App\Entity\Product;
use App\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;

class ModeratedProductEvent extends Event
{
    public const NAME = 'product.moderated';
    private $product;
    private $url;

    public function __construct(Product $product, $url)
    {
        $this->product = $product;
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    public function getProduct(){

        return $this->product;
    }

    public function getUser() : User {
        return $this->product->getUser();
    }
}