<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\Persistence\ManagerRegistry;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends NestedTreeRepository
{

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em,  $em->getClassMetadata(Category::class));
    }

    /**
      * @return Category[] Returns an array of Category objects
      */
//    public function findByNameField($value)
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.name = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

    public function findProductTypesByCatLvl($value){
        return $this->createQueryBuilder('c')
            ->select('p.name')
            ->where('c.lvl = :val')
            ->setParameter('val', $value)
            ->join('c.productTypes', 'p',  \Doctrine\ORM\Query\Expr\Join::WITH)
            ->getQuery()
            ->getArrayResult();

    }

    public function findCategoriesByLvl($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.lvl = :val')
            ->select('c.id, c.name')
            ->orderBy('c.sort_order','asc')
            ->setParameter('val', $value)
            ->getQuery()
            ->getResult()
            ;
    }


    public function findByNameField($value): ?Category
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.name = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

}
