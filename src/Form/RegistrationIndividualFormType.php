<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\UserIndividual;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Required;

class RegistrationIndividualFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,  [
                'label' => 'Имя*',
                'attr' => [
                    'class' => 'input',
                    'placeholder' => 'Олег'
                ],
                'label_attr' => [
                    'class' => 'input-label'
                ]
            ])
          ;


        if ($options['edit_form']) {
            $builder
                ->add('phone', TextType::class,
                    [
                        'mapped' => false,
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => '7 (911) 555-55-55'
                        ],
                        'label' => 'Мобильный телефон',
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])  ->add('email', EmailType::class,
                    [
                        'label' => 'Почта*',
                        'mapped' => false,
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'example@yandex.ru',
                            'readonly' => true
                        ],
                        'constraints' => new \Symfony\Component\Validator\Constraints\Email(),
                        'label_attr' => [
                            'class' => 'input-label'
                        ]

                    ]
                ) ->add('password', PasswordType::class,
                    [
                        'label' => 'Новый пароль',
                        'mapped' => false,
                        'required'=> false,
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => '******'
                        ],
                        'label_attr' => [
                            'class' => 'input-label'
                        ]

                    ]
                )
            ->add('submit', SubmitType::class, [
                'label' => 'Сохранить',
                'attr' => [
                    'class' => 'btn-primary preview__add  mt-2'
                ]
            ]);
        } else {
            $builder
                ->add('phone', HiddenType::class,
                [
                    'data_class' => User::class,
                    'mapped' => false,
                    'label' => 'Мобильный телефон'
                ])
                ->add('confidential', CheckboxType::class,[
                    'attr' => [
                        'class' => 'check-input',
                        'checked' => true
                    ],
                    'label' => 'Я прочитал и принимаю политику конфеденциальности*',
                    'mapped' => false,
                    'constraints' => new Required(),
                    'label_attr' => [
                        'class' => 'checkbox-label'
                    ]
                ])
                ->add('services', CheckboxType::class,[
                    'attr' => [
                        'class' => 'check-input',
                        'checked' => true
                    ],
                    'label' => 'Я прочитал и принимаю общие условия agigroo.com*',
                    'mapped' => false,
                    'constraints' => new Required(),
                    'label_attr' => [
                        'class' => 'checkbox-label'
                    ]
                ])  ->add('email', EmailType::class,
                    [
                        'data_class' => User::class,
                        'label' => 'Почта*',
                        'mapped' => false,
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'example@yandex.ru'
                        ],
                        'constraints' => new \Symfony\Component\Validator\Constraints\Email(),
                        'label_attr' => [
                            'class' => 'input-label'
                        ]

                    ]
                )
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserIndividual::class,
            'edit_form' => false
        ]);
    }
}
