<?php

namespace App\Repository;

use App\Entity\UserIndividual;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserIndividual|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserIndividual|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserIndividual[]    findAll()
 * @method UserIndividual[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserIndividualRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserIndividual::class);
    }

    // /**
    //  * @return UserIndividual[] Returns an array of UserIndividual objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserIndividual
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
