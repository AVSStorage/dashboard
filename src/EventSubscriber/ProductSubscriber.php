<?php

namespace App\EventSubscriber;

use App\Event\ModeratedProductEvent;
use App\Event\NeedUpdateProductEvent;
use App\Event\NewProductEvent;
use App\Event\NotModeratedProductEvent;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ProductSubscriber implements EventSubscriberInterface
{


    /**
     * @var MailerInterface
     */
    private $mailer;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(MailerInterface $mailer, LoggerInterface $logger)
    {
        $this->mailer = $mailer;
        $this->logger = $logger;
    }

    public function getTemplate($subject, $context, $email , $template = 'email/product/new-product.html.twig') {
        return (new TemplatedEmail())
            ->from(new Address('bh41574@piter3.dns-rus.net', 'Algroo.com Платформа объявлений'))
            ->to($email)
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($context);
    }

    public function onProductNew(NewProductEvent $event)
    {
        $product = $event->getProduct();
        $user = $event->getUser();
        $url = $event->getUrl();
        $email = $this->getTemplate('Создание нового объявления', [
            'message' => 'Ваше объявление было успешно создано!',
            'url' => $url
        ], $user->getEmail());

        try {
            $this->mailer->send($email);
        } catch (TransportException $exception) {
            $this->logger->error('Message new product not send');
        }


    }

    public function onProductNotModerated($event) {
        $product = $event->getProduct();
        $user = $event->getUser();
        $url = $event->getUrl();
        $reason = $event->getReason();
        $email = $this->getTemplate('Изменение статуса объявления', [
            'message' => 'Ваше объявление не прошло модерацию. Причина: '. $reason,
            'url' => $url
        ], $user->getEmail());


        try {
            $this->mailer->send($email);
        } catch (TransportException $exception) {
            $this->logger->error('Message product not  moderated not send');
        }
    }

    public function onProductModerated(ModeratedProductEvent $event) {
        $product = $event->getProduct();
        $user = $event->getUser();
        $url = $event->getUrl();
        $email = $this->getTemplate('Изменение статуса объявления', [
            'message' => 'Ваше объявление успешно прошло модерацию ',
            'url' => $url
        ], $user->getEmail());

        try {
            $this->mailer->send($email);
         } catch (TransportException $exception) {
            $this->logger->error('Message product  moderated not send');
        }
    }

    public function onProductNeedUpdate(NeedUpdateProductEvent $event) {
        $products = $event->getProducts();
        $email = $event->getEmail();

        $message =
           "Уважаемый клиент мы внимательно следим за актуальность информации на сервисе.".
           ( count($products) > 1 ? 'Ваши объявления' : 'Ваше объявление') ." не обновлялось 30 дней. 
           Если ".( count($products) > 1 ? 'они' : 'оно') ." ещё ".( count($products) > 1 ? 'актуальны' : 'актуально') ." обновите его в личном кабинете. ";
        $email = $this->getTemplate('Изменение статуса объявления', [
            'message' => $message,
            'products' => $products
        ], $email, 'email/product/update-ad.html.twig');
        try {
            $this->mailer->send($email);
        } catch (TransportException $exception) {
            $this->logger->error('Message product need update not send');
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            'product.new' => 'onProductNew',
            'product.not_moderated' => 'onProductNotModerated',
            'product.moderated' => 'onProductModerated',
            'product.need_update' => 'onProductNeedUpdate'
        ];
    }
}
