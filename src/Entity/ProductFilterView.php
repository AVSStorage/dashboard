<?php

namespace App\Entity;

use App\Repository\ProductFilterViewRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductFilterViewRepository::class, readOnly=true)
 * @ORM\Table(name="product_view")
 */
class ProductFilterView
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", name="category_id")
     */
    private $category;

    /**
     * @ORM\Column(type="integer", name="filter_id")
     */
    private $filter;

    /**
     * @ORM\Column(type="integer")
     */
    private $value;

    /**
     * @ORM\Column(type="array",name="`values`")
     */
    private $values = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    public $lvl;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoryId(): ?int
    {
        return $this->category;
    }



    public function getFilterId(): ?int
    {
        return $this->filter;
    }



    public function getValue(): ?int
    {
        return $this->value;
    }



    public function getValues(): ?array
    {
        return $this->values;
    }



    public function getName(): ?string
    {
        return $this->name;
    }

}
