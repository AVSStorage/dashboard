<?php


namespace App\Event;

use App\Entity\Product;
use App\Entity\ProductStatuses;
use App\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;


class NotModeratedProductEvent extends Event
{
    public const NAME = 'product.not_moderated';
    private $product;
    private $url;
    private $reason;

    public function __construct(Product $product, $url, $reason)
    {
        $this->product = $product;
        $this->url = $url;
        $this->reason = $reason;
    }

    public function getStatus() {

        return $this->product->getStatus();
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getReason()
    {
        return $this->reason;
    }

    public function getUser() : User {
        return $this->product->getUser();
    }
    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

}