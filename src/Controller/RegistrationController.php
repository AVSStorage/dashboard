<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\UserIndividual;
use App\Entity\UserLegal;
use App\Form\RegistrationFormType;
use App\Form\RegistrationIndividualFormType;
use App\Form\RegistrationLegalFormType;
use App\Helpers\FormFieldHelper;
use App\Repository\UserRepository;
use App\Security\EmailVerifier;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class RegistrationController extends AbstractController
{
    private $emailVerifier;

    public function __construct(EmailVerifier $emailVerifier)
    {
        $this->emailVerifier = $emailVerifier;
    }

    public function generateRandomPassword() {
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $string = '';
        $max = strlen($characters) - 1;
        for ($i = 0; $i < 10; $i++) {
            $string .= $characters[mt_rand(0, $max)];
        }

        return $string;
    }

    /**
     * @Route("/register/ul", name="app_register_ul")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, UserRepository $userRepository): Response
    {
        $userLegal = new UserLegal();
        $user = new User();
        $userIndividual = new UserIndividual();
        $form = $this->createForm(RegistrationLegalFormType::class, $userLegal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {




            $userIndividual->setName( $form->get('name')->getData());
            $user->setUserIndividual($userIndividual);
            $user->setUserLegal($userLegal);
            $user->setEmail( $form->get('email')->getData());
            $user->setRoles(['ROLE_USER', $user::ROLE_LEGAL]);

            $user->setPhone($form->get('phone')->getData());

            $password = $this->generateRandomPassword();
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $password
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->persist($userIndividual);
            $entityManager->persist($userLegal);
            $entityManager->flush();



                // generate a signed url and email it to the user
                $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                    (new TemplatedEmail())
                        ->from(new Address('bh41574@piter3.dns-rus.net', 'Algroo.com Платформа объявлений'))
                        ->to($user->getEmail())
                        ->subject('Подтверждение аккаунта')
                        ->htmlTemplate('registration/confirmation_email.html.twig')
                        ->context([
                            'password' => $password
                        ])
                );

            $this->addFlash('success', 'Вы были зарегестрированы как юридическое лицо, для подтверждения аккаунта проверьте Вашу почту!');
            // do anything else you need here, like send an email

            return $this->redirectToRoute('app_login');
        }

        return $this->render('registration/ul/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/register/person", name="app_register_person")
     */
    public function registerPerson(Request $request,  UserPasswordEncoderInterface $passwordEncoder, UserRepository $userRepository) {
        $user = new User();
        $userIndividual = new UserIndividual();
        $form = $this->createForm(RegistrationIndividualFormType::class, $userIndividual);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            if ($userRepository->findOneBy(['email' => $form->get('email')->getData()])){
                return $this->render('registration/person/register.html.twig', [
                    'form' => $form->createView(),
                    'error' => true,
                    'error_text' => 'Данная почта уже существует'
                ]);
            }
            $userIndividual->setName( $form->get('name')->getData());
            $user->setEmail( $form->get('email')->getData());
            $user->setRoles(['ROLE_USER', $user::ROLE_INDIVIDUAL]);
            $user->setPhone($form->get('phone')->getData());
            $user->setUserIndividual($userIndividual);

            $password = $this->generateRandomPassword();
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $password
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->persist($userIndividual);
            $entityManager->flush();


            // generate a signed url and email it to the user

                $this->emailVerifier->sendEmailConfirmation('app_verify_email', $user,
                    (new TemplatedEmail())
                        ->from(new Address('bh41574@piter3.dns-rus.net', 'Algroo.com Платформа объявлений'))
                        ->to($user->getEmail())
                        ->subject('Подтверждение аккаунта')
                        ->htmlTemplate('registration/confirmation_email.html.twig')
                        ->context([
                            'password' => $password
                        ])
                );



            $this->addFlash('success', 'Вы были зарегестрированы как физическое лицо, для подтверждения аккаунта проверьте Вашу почту!');
            // do anything else you need here, like send an email

            return $this->redirectToRoute('app_login');
        }





        return $this->render('registration/person/register.html.twig', [
            'form' => $form->createView(),
            'error' => false,
            'error_text' => ''
        ]);
    }

    /**
     * @Route("/verify/email", name="app_verify_email")
     */
    public function verifyUserEmail(Request $request): Response
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');


        // validate email confirmation link, sets User::isVerified=true and persists
        try {
            $this->emailVerifier->handleEmailConfirmation($request, $this->getUser());
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('verify_email_error', $exception->getReason());


            return $this->redirectToRoute('app_login');
        }

        // @TODO Change the redirect on success and handle or remove the flash message in your templates
        $this->addFlash('success', 'Ваш аккаунт верифицирован.');

        return $this->redirectToRoute('app_login');
    }
}
