<?php

namespace App\Command;

use App\Event\NeedUpdateProductEvent;
use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Router;

class UpdateAdsCommand extends Command
{
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(string $name = null, ProductRepository $productRepository,  \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher, UrlGeneratorInterface $router, UserRepository $userRepository)
    {
        parent::__construct($name);
        $this->productRepository = $productRepository;
        $this->eventDispatcher = $eventDispatcher;

        $this->router = $router;
        $this->userRepository = $userRepository;
    }

    protected static $defaultName = 'app:update-ads';

    protected function configure()
    {
        $this
            ->setDescription('Notify users to update ads')
            ->addOption('days', null, InputOption::VALUE_OPTIONAL, 'Number of active ads status days before update', 30)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $days = [];
        $products = $this->productRepository->findAll();
        $products = new ArrayCollection($products);
        $productsGroupByUser = [];
        $products->map(function($item) use (&$productsGroupByUser) {
            $productsGroupByUser[$item->getUser()->getId()][] = $item;
        });
        foreach ($productsGroupByUser as $userId =>  $userProducts) {
            foreach ($userProducts as $product) {
                $updatedDate = $product->getUpdatedAt();
                if (!$updatedDate) {
                    $updatedDate = $product->getCreatedAt();
                }
                $nowDate = new \DateTime('now');
                $diff = $nowDate->diff($updatedDate);

                if ($diff->days > 25) {
                    $days[$userId][] = $product;


                }
            }
        }





        foreach ($days as $userId => $userProducts) {
            $user = $this->userRepository->findOneBy(['id' => $userId]);
            $this->eventDispatcher->dispatch(new NeedUpdateProductEvent($user, $userProducts), NeedUpdateProductEvent::NAME);
        }


        if ($input->getOption('days')) {
            $io->note($input->getOption('days'));
        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
