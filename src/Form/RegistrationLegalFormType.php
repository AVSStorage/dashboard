<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\UserIndividual;
use App\Entity\UserLegal;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Required;

class RegistrationLegalFormType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        if ($options['edit_form']) {
            $builder
                ->add('company_name', TextType::class,[
                    'label' => 'Название компании*',
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => 'Омега'
                    ],
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])

                ->add('sex', ChoiceType::class, [
                    'label' => 'Пол',
                    'choices'  => [
                        'Мужской' => 1,
                        'Женский' => 2
                    ],
                    'attr' => [
                        'class' => 'registration__radios'
                    ],
                    'label_attr' => [
                        'class' =>   'input-label'
                    ],
                    'choice_attr' => [
                        'class' => 'radio-label'
                    ],
                    'required' => true,
                    'expanded' => true,
                ]) ->add('email', EmailType::class,
                    [
                        'label' => 'Почта*',
                        'mapped' => false,
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'example@yandex.ru'
                        ],
                        'label_attr' => [
                            'class' => 'input-label'
                        ]

                    ]
                )
                ->add('name', TextType::class, [
                    'label' => 'Имя*',
                    'mapped' => false,
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => 'Олег'
                    ],
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])

                ->add('surname', TextType::class, [
                    'label' => 'Фамилия*',
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => 'Иванов'
                    ],
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])
                ->add('password', PasswordType::class,
                    [
                        'label' => 'Новый пароль',
                        'mapped' => false,
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => '******'
                        ],
                        'required'=> false,
                        'label_attr' => [
                            'class' => 'input-label'
                        ]

                    ])
                ->add('phone', TextType::class,
                    [
                        'label' => 'Номер телефона',
                        'mapped' => false,
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Олег'
                        ],
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])
                ->add('work_number_additive', HiddenType::class,
                    [
                        'mapped' => false,
                        'required' => false,
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Олег'
                        ],
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])
                ->add('work_number', HiddenType::class)
                ->add('country', TextType::class,
                    [
                        'label' => 'Страна',
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Россия',

                        ],
                        'required' => false,
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])
                ->add('index', NumberType::class, [
                    'label' => 'Индекс',
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => '456 344',

                    ],
                    'html5' => true,
                    'required' => false,
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])
                ->add('address', TextType::class,
                    [
                        'label' => 'Адрес фактический*',
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Москва, ул. Тургенева 25'
                        ],
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])


                ->add('INN', NumberType::class, [
                    'label' => 'Налоговый идентификатор*',
                    'constraints' => new Length(['min' => 10, 'max' => 10]),
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => '3676847878'
                    ],
                    'html5' => true,
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])
                ->add('legal_address', TextType::class,
                    [
                        'label' => 'Адрес юридический*',
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Москва, ул. Тургенева 25'
                        ],
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])
                ->add('description', TextareaType::class,
                    [
                        'label' => 'Описание',
                        'attr' => [
                            'class' => 'textarea',
                            'rows' => 9,
                            'placeholder' => 'Кратко опишите компанию',

                        ],
                        'required' => false,
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])
                ->add('company_name', TextType::class,[
                    'label' => 'Название компании*',
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => 'Омега'
                    ],
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])


                ->add('surname', TextType::class, [
                    'label' => 'Фамилия*',
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => 'Иванов'
                    ],
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])

                ->add('country', TextType::class,
                    [
                        'label' => 'Страна',
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Россия',

                        ],
                        'required' => false,
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])
                ->add('index', NumberType::class, [
                    'label' => 'Индекс',
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => '456 344',

                    ],
                    'html5' => true,
                    'required' => false,
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])
                ->add('address', TextType::class,
                    [
                        'label' => 'Адрес фактический*',
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Москва, ул. Тургенева 25'
                        ],
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])


                ->add('INN', NumberType::class, [
                    'label' => 'Налоговый идентификатор*',
                    'constraints' => new Length(['min' => 10, 'max' => 10]),
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => '3676847878'
                    ],
                    'html5' => true,
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])
                ->add('legal_address', TextType::class,
                    [
                        'label' => 'Адрес юридический*',
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Москва, ул. Тургенева 25'
                        ],
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])
                ->add('description', TextareaType::class,
                    [
                        'label' => 'Описание',
                        'attr' => [
                            'class' => 'textarea',
                            'rows' => 9,
                            'placeholder' => 'Кратко опишите компанию',

                        ],
                        'required' => false,
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])


                ->add('person_number_additive', HiddenType::class,[
                    'mapped' => false,
                    'required' => false,
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => 'Олег'
                    ],
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])
                ->add('submit', SubmitType::class, [
                    'label' => 'Сохранить',
                    'attr' => [
                        'class' => 'btn-primary preview__add  mt-2'
                    ]
                ])
           ;
        } else {
            $builder
                ->add('company_name', TextType::class,[
                    'label' => 'Название компании*',
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => 'Омега'
                    ],
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])


                ->add('surname', TextType::class, [
                    'label' => 'Фамилия*',
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => 'Иванов'
                    ],
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])
                ->add('work_number', HiddenType::class)
                ->add('country', TextType::class,
                    [
                        'label' => 'Страна',
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Россия',

                        ],
                        'required' => false,
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])
                ->add('index', NumberType::class, [
                    'label' => 'Индекс',
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => '456 344',

                    ],
                    'html5' => true,
                    'required' => false,
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])
                ->add('address', TextType::class,
                    [
                        'label' => 'Адрес фактический*',
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Москва, ул. Тургенева 25'
                        ],
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])


                ->add('INN', NumberType::class, [
                    'label' => 'Налоговый идентификатор*',
                    'constraints' => new Length(['min' => 10, 'max' => 10]),
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => '3676847878'
                    ],
                    'html5' => true,
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])
                ->add('legal_address', TextType::class,
                    [
                        'label' => 'Адрес юридический*',
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Москва, ул. Тургенева 25'
                        ],
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])
                ->add('description', TextareaType::class,
                    [
                        'label' => 'Описание',
                        'attr' => [
                            'class' => 'textarea',
                            'rows' => 9,
                            'placeholder' => 'Кратко опишите компанию',

                        ],
                        'required' => false,
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])
                ->add('company_name', TextType::class,[
                    'label' => 'Название компании*',
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => 'Омега'
                    ],
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])


                ->add('surname', TextType::class, [
                    'label' => 'Фамилия*',
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => 'Иванов'
                    ],
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])
                ->add('work_number', HiddenType::class)
                ->add('country', TextType::class,
                    [
                        'label' => 'Страна',
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Россия',

                        ],
                        'required' => false,
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])
                ->add('index', NumberType::class, [
                    'label' => 'Индекс',
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => '456 344',

                    ],
                    'html5' => true,
                    'required' => false,
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])
                ->add('address', TextType::class,
                    [
                        'label' => 'Адрес фактический*',
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Москва, ул. Тургенева 25'
                        ],
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])


                ->add('INN', NumberType::class, [
                    'label' => 'Налоговый идентификатор*',
                    'constraints' => new Length(['min' => 10, 'max' => 10]),
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => '3676847878'
                    ],
                    'html5' => true,
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])
                ->add('legal_address', TextType::class,
                    [
                        'label' => 'Адрес юридический*',
                        'attr' => [
                            'class' => 'input',
                            'placeholder' => 'Москва, ул. Тургенева 25'
                        ],
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])
                ->add('description', TextareaType::class,
                    [
                        'label' => 'Описание',
                        'attr' => [
                            'class' => 'textarea',
                            'rows' => 9,
                            'placeholder' => 'Кратко опишите компанию',

                        ],
                        'required' => false,
                        'label_attr' => [
                            'class' => 'input-label'
                        ]
                    ])
                ->add('email', EmailType::class,
                [
                    'data_class' => User::class,
                    'label' => 'Почта*',
                    'mapped' => false,
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => 'example@yandex.ru',
                        'readonly' => true
                    ],
                    'label_attr' => [
                        'class' => 'input-label'
                    ]

                ]
            )
                ->add('phone', HiddenType::class,
                    [
                        'data_class' => User::class,
                        'mapped' => false
                    ])
                ->add('work_number_additive', HiddenType::class,
                    [
                    ])
                ->add('person_number_additive', HiddenType::class,[
                ])
                ->add('name', TextType::class, [
                    'data_class' => UserIndividual::class,
                    'label' => 'Имя*',
                    'mapped' => false,
                    'attr' => [
                        'class' => 'input',
                        'placeholder' => 'Олег'
                    ],
                    'label_attr' => [
                        'class' => 'input-label'
                    ]
                ])
                ->add('confidential', CheckboxType::class,[
                    'attr' => [
                        'class' => 'check-input',
                        'checked' => true
                    ],
                    'mapped' => false,
                    'constraints' => new Required(),
                    'label_attr' => [
                        'class' => 'checkbox-label'
                    ]
                ])
                ->add('services', CheckboxType::class,[
                    'attr' => [
                        'class' => 'check-input',
                        'checked' => true
                    ],
                    'mapped' => false,
                    'constraints' => new Required(),
                    'label_attr' => [
                        'class' => 'checkbox-label'
                    ]
                ])
                ->add('show_work_number_additive', CheckboxType::class,[
                    'attr' => [
                        'class' => 'check-input',
                        'checked' => true

                    ],
                    'required' => false,
                    'label_attr' => [
                        'class' => 'checkbox-label'
                    ]
                ])
                ->add('show_person_number_additive', CheckboxType::class,[
                    'attr' => [
                        'class' => 'check-input',
                        'checked' => true
                    ],
                    'required' => false,
                    'label_attr' => [
                        'class' => 'checkbox-label'
                    ]
                ]) ->add('sex', ChoiceType::class, [
                    'choices'  => [
                        'Мужской' => 1,
                        'Женский' => 2
                    ],
                    'attr' => [
                        'class' => 'registration__radios'
                    ],
                    'choice_attr' => [
                        'class' => 'radio-label'
                    ],
                    'required' => true,
                    'expanded' => true,
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserLegal::class,
            'edit_form' => false
        ]);
    }
}
