<?php

namespace App\Repository;

use App\Entity\CategoryStatuses;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategoryStatuses|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoryStatuses|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoryStatuses[]    findAll()
 * @method CategoryStatuses[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryStatusesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoryStatuses::class);
    }

    // /**
    //  * @return CategoryStatuses[] Returns an array of CategoryStatuses objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoryStatuses
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
