<?php

namespace App\Controller\API;

use App\Entity\ClickCounter;
use App\Entity\UserIndividual;
use App\Form\LoginFormType;
use App\Form\RegistrationIndividualFormType;
use App\Repository\ProductRepository;
use App\Serializer\Normalizer\FormNormalizer;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Swagger\Annotations as SWG;
use Tchoulom\ViewCounterBundle\Counter\ViewCounter;
use Tchoulom\ViewCounterBundle\Finder\StatsFinder;
use Tchoulom\ViewCounterBundle\Statistics\Statistics;

class APIDemoController extends AbstractController
{
    private $serializer;

    public function __construct()
    {
        $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/api/house", name="api_house", format="json", methods={"GET"}, condition="request.isXmlHttpRequest()")
     * @SWG\Get(
     *     path="/api/house",
     *     summary="Get movies",
     *     description="Get movies",
     *     @SWG\Response(
     *     response=200,
     *     description="Returns the rewards of an user",
     *     @SWG\Schema(
     *         type="array",
     *     )
     *     )
     * )
     */
    public function index(Request $request)
    {
        $userIndividual = new UserIndividual();
        $form = $this->createForm(RegistrationIndividualFormType::class, $userIndividual);
        $fields = $form->createView()->children;

        $normalizers = new FormNormalizer(new ObjectNormalizer());
        $data = $normalizers->normalize($form, 'json', ['neededFieldsArray' => ['id', 'full_name', 'label', 'label_attr', 'required', 'attr'], 'fields' => $fields]);

        $view = $this->serializer->serialize($data, 'json');
        return JsonResponse::fromJsonString($view);

    }

    /**
     * @Route("/click/add", name="api_add_click",  methods={"POST"} , format="json", condition="request.isXmlHttpRequest()")
     */
    public function addClick(Request $request, StatsFinder $statsFinder, Statistics $statistics, ViewCounter $viewCounter, EntityManagerInterface $em, ProductRepository $productRepository)
    {

        $productId = $request->get('id');
        $product = $productRepository->findOneBy(['id' => $productId]);


        dd( $viewCounter->saveView($product, 1));

        $viewcounter = !empty($product->getClickCounters()->getValues()) ? $product->getClickCounters()->get(0) : new ClickCounter();




        $clicks = $product->getClicks();
        $viewcounter->setIp($request->getClientIp());


        $viewcounter->setProduct($product);
        $viewcounter->setViewDate(new \DateTime('now'));
//        $statistics->register($product);
        $product->setClicks($clicks ? $clicks + 1 : 1);
        $product->addClickCounter($viewcounter);

        $em->persist($viewcounter);
        $em->persist($product);
        $em->flush();




        return new JsonResponse(['success' => true]);
    }
}
