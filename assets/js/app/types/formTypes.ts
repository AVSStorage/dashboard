export type MobileInputType = {
    label: string | undefined,
    inputId: string,
    inputName: string,
    checkboxOptions: string,
    showCheckbox: boolean
}