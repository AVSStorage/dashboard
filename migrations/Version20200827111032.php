<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200827111032 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');


        $this->addSql('ALTER TABLE "user" ADD user_individual_id INT DEFAULT NULL');

        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D64940A9B15D FOREIGN KEY (user_individual_id) REFERENCES user_individual (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D64940A9B15D ON "user" (user_individual_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');

        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D64940A9B15D');
        $this->addSql('DROP INDEX UNIQ_8D93D64940A9B15D');
        $this->addSql('ALTER TABLE "user" DROP user_individual_id');

    }
}
