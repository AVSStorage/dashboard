<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\CategoryStatuses;
use App\Entity\Filter;
use App\Entity\Product;
use App\Entity\ProductStatuses;
use App\Repository\CategoryRepository;
use App\Repository\FilterRepository;
use App\Repository\FilterValuesRepository;
use App\Repository\ProductFilterRepository;
use App\Repository\ProductFilterViewRepository;
use App\Repository\ProductRepository;
use App\Repository\ProductTypeRepository;
use App\Service\CategoryService;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class CategoryController extends AbstractController
{


    /**
     * @Route("/catalog/{category_slug}", priority=10, name="category_page", requirements={"category_slug" = ".+"})
     *
     */
    public function getCategory(string $category_slug,
                                Request $request,
                                EntityManagerInterface $entityManager,
                                CategoryRepository $categoryRepository,
                                CategoryService $categoryService,
                                PaginatorInterface $paginator,
                                ProductRepository $productRepository,
                                ProductTypeRepository $productTypeRepository,
                                FilterRepository $filterRepository,
                                ProductFilterViewRepository $productFilterViewRepository,
                                SessionInterface $session,
                                FilterValuesRepository $filterValuesRepository
    )
    {


        $category_slugs = explode('/', $category_slug);
        $category_slug = end($category_slugs);
        $category = $categoryRepository->findOneBy(['slug' => $category_slug]);

        if (!$category) {
            throw new NotFoundHttpException();
        }


        $query = $productRepository->getProductsByIdQuery()->andWhere('p.status = :status')->setParameter('status', ProductStatuses::STATUS_ACTIVE);


        $cat = [];

        foreach ($category_slugs as $slug) {

            $categories = $categoryService->getProductsFromCategory($categoryRepository->findOneBy(['slug' => $slug]), $query, false)[1];
            if (!empty($categories)) {
                $cat[] = $categories;
            }

        }


        list($products, $categories, $filters) = $categoryService->getProductsFromCategory($category, $query);


        $filterSelected = $request->query->all();


        $exclude = ['filter_form_type', 'sort','direction','page'];

        if ($filterSelected && !isset($filterSelected['filter_form_type']) && !$request->query->has('reset')) {



            $rsm = new ResultSetMapping;
            $rsm->addEntityResult('App\Entity\ProductFilterView', 'p');
            $rsm->addFieldResult('p', 'id', 'id');
            $query = $entityManager->createNativeQuery('SELECT distinct p.id FROM product_view p', $rsm);
            $queryText = 'SELECT distinct p.id FROM product_view p';
            $myQuery = '';


            foreach ($filterSelected as $key => $filter) {

                if ($filter !== '' && !in_array($key, $exclude)) {
                    $ids = $categoryRepository->getPathQueryBuilder($category)->select('node.id')->getQuery()->getResult();
                    $ids = array_map(function ($left) {

                        return $left['id'];
                    }, $ids);

                    $children = $categoryRepository->getChildrenQueryBuilder($category)->select('node.id')->getQuery()->getResult();

                    $children = array_map(function ($left) {

                        return $left['id'];
                    }, $children);

                    $ids = array_merge($children, $ids);

                    if ($key !== 'model') {



                    $f = $filterRepository->createQueryBuilder("filter")->leftJoin('filter.category', 'c')->where(" filter.values LIKE '%" . $filter . "%' AND c.id IN (" . implode(',', $ids) . ")")->getQuery()->getOneOrNullResult();


                    $id = array_search($filter, $f->getValues());

                    $myQuery = ' JOIN (SELECT * FROM product_view WHERE slug = :filterId' . md5($key . $id) . ' and value = :value' . md5($key . $id) . ' and lvl >= ' . $category->getLvl() . ' ) as m' . md5($key . $id) . ' ON m' . md5($key . $id) . '.id = p.id';
                    $queryText .= $myQuery;
                    $query->setParameter('filterId' . md5($key . $id), $key);
                    $query->setParameter('value' . md5($key . $id), $id);


                    } else {

                        $model = $filterValuesRepository->findOneBy(['name' => $filter]);
                        if ($this->getParameter('current_db') === 'postgres') {
                            $myQuery = ' JOIN (SELECT * FROM product_view WHERE name=\'Модель\' and  additive->>\'' . $model->getId() . '\' IS NOT NULL ) as m' . $model->getId() . ' ON m' . $model->getId() . '.id = p.id';
                        } else {
                            $myQuery = ' JOIN (SELECT * FROM product_view WHERE name=\'Модель\' and JSON_EXTRACT(additive, "$.'.$model->getId().'")  IS NOT NULL ) as m' . $model->getId() . ' ON m' . $model->getId() . '.id = p.id';
                        }
                        $queryText .= $myQuery;
                    }
                }
            }


            $query->setSQL($queryText);

            if ($queryText !== 'SELECT distinct p.id FROM product_view p') {
                $ids = [];
                $productViews = $query->getResult();
                foreach ($productViews as $productView) {
                    $ids[] = $productView->getId();
                }


                $products = $productRepository->createQueryBuilder('p')->where('p.id IN (:ids)')->setParameter('ids', $ids)->andWhere('p.status = :status')->setParameter('status', ProductStatuses::STATUS_ACTIVE);

                $filters = $categoryService->loopProducts($products->getQuery()->getResult());
            }

            $filtersSelected = array_values($filterSelected);

        } else {
            $filtersSelected = [];
        }



        $type = isset($filterSelected['filter_form_type']) ? $filterSelected['filter_form_type'] : null;


        if ($type && $type !== '') {
          //  $session->set('filter_form_type', $type);

            $products = $products->leftJoin('p.type', 'pt')->andWhere('pt.slug = :slug')
                ->setParameter('slug', $type);
        } elseif (!$type) {
            //$type = $session->get('filter_form_type');
        }



        $pagination = $paginator->paginate(
            $products, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            8 /*limit per page*/,
            array('wrap-queries' => true)
        );


        uasort($filters, function ($left, $right) {
            if (is_null($left['sort_order'])) return 1;
            if (is_null($right['sort_order'])) return -1;
            return $left['sort_order'] > $right['sort_order'];
        });




        $pagination->setTemplate('@KnpPaginator/Pagination/twitter_bootstrap_v4_pagination.html.twig');

        $pagination->setSortableTemplate('@KnpPaginator/Pagination/twitter_bootstrap_v3_sortable_link.html.twig');


        return $this->render('category/index.html.twig', [
            'categories' => $categoryRepository->findBy(['lvl' => 0], ['sort_order' => 'DESC']),
            'products' => $pagination,
            'children' => $categories,
            'cats' => $cat,
            'selectedType' => $type,
            'slugs' => $category_slugs,
            'category_slug' => $category_slug,
            'filtersSelected' => $filtersSelected,
            'filters' => $filters,
            'productTypes' => is_array($products) ? [] : $products->leftJoin('p.type', 't')->select('distinct t.name, t.slug')->getQuery()->getResult(),
            'controller_name' => 'CategoryController',
        ]);
    }


    /**
     * @Route("/category/{id}/delete", name="category_delete")
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, CategoryService $categoryService, CategoryRepository $categoryRepository, ProductRepository $productRepository, EntityManagerInterface $entityManager, Security $security)
    {

        $category = $categoryRepository->findOneBy(['id' => $request->attributes->get('id')]);
        $parentCategory = $category->getParent();

        $categoryService->deleteCategories($category, $parentCategory);

        return new RedirectResponse('/admin/categories');

    }
}
