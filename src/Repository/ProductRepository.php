<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\ProductStatuses;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

     /**
      * @return Product[] Returns an array of Product objects
      */

    public function findAllProductsWithUserInfo($count) : QueryBuilder
    {

        return $this->createQueryBuilder('p')
            ->leftJoin('App\Entity\AllUsers','u', Join::WITH, 'p.user = u.id')
            ->select('u.name as user_info, p as product')
            ->where('p.status = :status')
            ->setParameter('status', ProductStatuses::STATUS_ACTIVE)
            ->orderBy('p.createdAt', 'DESC')
            ->setMaxResults($count)
        ;


    }

    public function getGroupByUserProducts() {
        return $this->createQueryBuilder('p')
            ->getQuery()
            ->getResult()
            ;
    }


    public function getPaginateItemsQuery(){
        return $this->createQueryBuilder('p')
            ->orderBy('p.id', 'DESC')
            ->setFirstResult(0)
          ;
    }


    public function getPaginateItemsWithCategoriesQuery(){
        return $this->createQueryBuilder('p')
            ->leftJoin('App\Entity\Category','c',JOIN::WITH,'p.category = c.id')
            ->orderBy('p.id', 'DESC')

            ;
    }

    public function getTest($lvl) {
        return $this->getPaginateItemsWithCategoriesQuery()
            ->where('c.parent = :lvl')
            ->setParameter('lvl', $lvl);
    }

    public function getPaginateItemsBySlug($value) {
        return $this->getPaginateItemsWithCategoriesQuery()
            ->andWhere('c.slug = :val')
            ->setParameter('val', $value)
            ->setFirstResult(0);
    }

    public function getProductsByIdQuery() {
        return $this->createQueryBuilder('p')

            ->where('p.id IN (:id)')
            ;
    }


    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
