import {Module, Mutation, Action} from 'vuex-module-decorators'
import axios, {AxiosError, AxiosResponse, AxiosRequestConfig} from 'axios'
import ErrorHandlerVuexClass from './ErrorClass'

interface ApiError {
    config: AxiosRequestConfig;
    code?: string;
    response?: AxiosResponse<string>;
    isAxiosError: boolean;
    toJSON: () => object;
}

interface ServerData {
    data: string
}

export type PhotoType = {
    id: number,
    value: File | string,
    uploaded: boolean,
    original: string;
    action: string;
}

export type PhotoArrayType = Array<PhotoType>

@Module({namespaced: true})
class ImageModel extends ErrorHandlerVuexClass {

    photosArray: PhotoArrayType = [];
    action = 'new';
    public productId!: number;

    @Mutation
    public initializePhoto(itemsCount: number) {
        [...Array(itemsCount).keys()].forEach((_ :number, index: number) => {
            this.photosArray.push({
               id: index + 1,
               value: '',
               uploaded: false,
               original: '',
               action: 'new'
            });
        })

    }

    @Mutation
    public setPhotosArray({photos, productId} : {photos: PhotoArrayType, productId: number}) {
        this.photosArray = photos;
        this.action = 'edit';
        this.productId = productId;
    }

    @Mutation
    public setPhoto({id, value, original, action} :{id: number, value: string, original: string, action:string}) {
        const index = this.photosArray.findIndex((photo: PhotoType) => photo.id === id);
        if (index !== -1) {
            const {id} = this.photosArray[index];
            this.photosArray.splice(index, 1,
                { id, uploaded: true, value, original, action })
        }
    }


    @Mutation
    public deletePhoto(id: number) {
        const index = this.photosArray.findIndex((photo: PhotoType) => photo.id === id);
        if (index !== -1) {
            const {id} = this.photosArray[index];
            this.photosArray.splice(index, 1,
                { id, uploaded: false, value: '', original: '', action: 'new' })
        }
    }

    @Action({rawError: true})
    public initializePhotosArray(itemsCount: number) {
        this.context.commit('initializePhoto', itemsCount + 1)
    }

    @Action({rawError: true})
    public setPhotosArrayAction({photos, productId} : {photos: PhotoArrayType, productId: number}) {
        this.context.commit('setPhotosArray', {photos, productId})
    }

    @Action({rawError: true})
    public uploadPhoto({id, value} : {id: number, value: File}) {
        const data = new FormData();
        // @ts-ignore
        data.append('image_form[image]', value);
        if (this.productId) {
            // @ts-ignore
            data.append('productId', this.productId);
            data.append('action', this.action );
        }

        axios.request<ServerData>({
            url: '/api/product/image/upload',
            method: 'POST',
            data: data,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            transformResponse: (r) => {
                return r
            }
        }).then((response: AxiosResponse) => {
            // `response` is of type `AxiosResponse<ServerData>`
            const {data} = response;

            this.context.dispatch('resetError');
            this.context.commit('setPhoto', {id,
                value: JSON.parse(data).full_path,
                original: JSON.parse(data).original })

        }).catch((error: AxiosError<ApiError>) => {
            // @ts-ignore
            const { data } = error.response;

            this.context.commit('setError', JSON.parse(data).message)
        })

    }

    @Action({rawError: true})
    public deletePhotoAction(id: number) {
        const data = new FormData();
        const image = this.photosArray.find((photo: PhotoType) => photo.id === id);
        // @ts-ignore
        data.append('image', image.original );
        // @ts-ignore
        data.append('action', this.action );
        if (this.productId) {
            // @ts-ignore
            data.append('productId', this.productId);
        }

        // @ts-ignore
        if (image.value) {
            axios.request<ServerData>({
                url: '/api/product/image/delete',
                method: 'POST',
                data: data,
                headers: {'X-Requested-With': 'XMLHttpRequest'},
                transformResponse: (r) => {
                    return r
                }
            }).then((response: AxiosResponse) => {
                // `response` is of type `AxiosResponse<ServerData>`
                const {data} = response;

                this.context.commit('deletePhoto', id)
            }).catch((error: AxiosError<ApiError>) => {
                this.context.commit('setError', error.message)
            })
        }

    }
}

export default ImageModel