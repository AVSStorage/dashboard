<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\CategoryStatuses;
use App\Entity\Filter;
use App\Entity\ProductType;
use Doctrine\DBAL\Types\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

            $builder
                ->add('category', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
                    'mapped' => false
                ])
                ->add('filters', EntityType::class, [
                    'class' => Filter::class,
                    'choice_label' => 'name',
                    'multiple' => true,
                    'label' => 'Выберите фильтры к категории',
                    'label_attr' => [
                        'class' => 'filters__label'
                    ],
                    'required' => false,
                    'attr' => [
                        'size' => 10,
                        'class' => 'filters__select'
                    ],
                    'mapped' => false
                ]);




            if ($options['lvl']){
                $builder
                    ->add('name', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
                        'label' => 'Название категории',
                        'label_attr' => [
                            'class' => 'filters__label'
                        ],
                        'required' => false,
                        'mapped' => true,
                        'attr' => [
                            'class' => 'input'
                        ]
                    ])
                    ->add('sort_order', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
                        'label' => 'Укажите порядок сортировки',
                        'label_attr' => [
                            'class' => 'filters__label'
                        ],
                        'mapped' => false,
                        'required' => false,
                        'attr' => [
                            'class' => 'input'
                        ]
                    ]);

            } else {
                $builder->add('name', ChoiceType::class, [
                    'label' => 'Название категории',
                    'label_attr' => [
                        'class' => 'filters__label'
                    ],
                    'required' => false,
                    'mapped' => false,
                    'attr' => [
                        'class' => 'input'
                    ]
                ]);

                $builder->get('name')->resetViewTransformers();
            }

        if ($options['show_parent'] && $options['lvl']) {
            $builder->add('parent_category', \Symfony\Component\Form\Extension\Core\Type\TextType::class, [
                'label' => 'Укажите ID родительской категории',
                'mapped' => false,
                'label_attr' => [
                    'class' => 'filters__label'
                ],
                'required' => false,
                'attr' => [
                    'class' => 'input'

                ]
            ]);
        }



    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
            'lvl' => false,
            'show_parent' => false
        ]);
    }
}
