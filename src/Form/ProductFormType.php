<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Filter;
use App\Entity\Product;
use App\Entity\ProductStatuses;
use App\Entity\ProductType;
use App\Repository\CategoryRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', TextType::class, [
                'mapped' => false,
                'attr' => [
                'class' => 'select-css filters__select',
                ],
                'label' => 'Выберите категорию',
                'label_attr' => [
                    'class' => 'filters__label'
                ],
            ])
            ->add('type', TextType::class, [
                'attr' => [
                    'class' => 'radio-label-button'
                ],
                'label_attr' => [
                    'class' => 'radio-label'
                ],
                'label' => 'Выбирите тип',
                'required' => true,
                'error_mapping' => true,
                'mapped' => false,
                'constraints' => new NotBlank([ 'message' => 'Тип объявление не может быть пустым'])
            ])
            ->add('filter', CollectionType::class, [
                'attr' => [
                    'class' => 'radio-label-button'
                ],
                'allow_extra_fields' => true,
                'label_attr' => [
                    'class' => 'radio-label'
                ],
                'label' => 'Выбирите фильтр',
                'required' => false,
                'mapped' => false
            ])
            ->add('name', TextType::class, [
                'label' => 'Название объявления',
                'attr' => [
                    'class' => 'input',
                    'placeholder' => 'Название'
                ],
                'label_attr' => [
                    'class' => 'filters__label'
                ]
            ])
            ->add('description',TextareaType::class, [
                'label' => 'Описание объявления',
                'label_attr' => [
                    'class' => 'filters__label'
                ],
                    'attr' => [
                        'class' => 'textarea',
                        'rows' => 16,
                        'placeholder' => 'Описание объявления',

                    ],
                    'required' => false
            ])
            ->add('price', NumberType::class,[
                'html5' => true,
                'attr' => [
                    'class' => 'input',
                    'placeholder' => 'Цена'
                ]
            ])
            ->add('address', TextType::class, [
                'attr' => [
                    'class' => 'input',
                    'placeholder' => 'Адрес'
                ]
            ])
        ;


        if (in_array('ROLE_ADMIN', $options['role'])) {

            $builder
                ->add('status', EntityType::class, [
                    'class' => ProductStatuses::class,
                    'choice_label' => 'name',
                    'label' => 'Статус объявления',
                    'label_attr' => [
                        'class' => 'filters__label'
                    ],
                    'required' => false,
                    'attr' => [
                        'class' => 'select-css filters__select'
                    ],
                ]);
            $builder->add('reason', TextareaType::class,[
                'label' => 'Причина отклонения модерации',
                'label_attr' => [
                    'class' => 'filters__label'
                ],
                'required' => false,
                'attr' => [
                    'class' => 'textarea',
                    'rows' => 4,

                ]
            ]);
        }

       // $builder->get('filter')->resetViewTransformers();





    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
            'role' => ['ROLE_USER']
        ]);
    }
}
