<?php

namespace App\Controller;

use App\Entity\ProductType;
use App\Repository\CategoryRepository;
use App\Repository\FilterRepository;
use App\Repository\FilterValuesRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class IndexController extends AbstractController
{
    const ITEMS_PER_PAGE = 8;
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request, ProductRepository $productRepository, EntityManagerInterface $entityManager,  PaginatorInterface $paginator, CategoryRepository $categoryRepository, FilterRepository $filterRepository, FilterValuesRepository $filterValuesRepository)
    {

        $products = $productRepository->findAllProductsWithUserInfo(self::ITEMS_PER_PAGE);

        $pagination = $paginator->paginate(
            $products, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            8 /*limit per page*/,
            array('wrap-queries' => true)
        );
         $productType = new ProductType();
         $productType->addCategory($categoryRepository->findOneBy(['name' => 'Услуги']));
         $productType->setName('Предлагаю услуги');
         $entityManager->persist($productType);
         $entityManager->flush();

        $links = [
            'transport' => $categoryRepository->findOneBy(['name' => 'Транспорт']),
            'rent' => $categoryRepository->findOneBy(['name' => 'Недвижимость']),
            'home' => $categoryRepository->findOneBy(['name' => 'Товары для дома']),
            'sport' => $categoryRepository->findOneBy(['name' => 'Спорт, отдых, хобби']),
            'animals' => $categoryRepository->findOneBy(['name' => 'Животные']),
            'clothes' => $categoryRepository->findOneBy(['name' => 'Одежда, обувь, аксессуары']),
            'beauty' =>  $categoryRepository->findOneBy(['name' => 'Красота и здоровье'])
        ];

        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
            'products' => $pagination,
            'links' => $links,
            'transport' => $categoryRepository->findOneBy(['name' => 'Транспорт']),
            'arenda' => $categoryRepository->findOneBy(['name' => 'Недвижимость'])
        ]);
    }
}
