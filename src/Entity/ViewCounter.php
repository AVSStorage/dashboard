<?php

namespace App\Entity;

use App\Repository\ViewCounterRepository;
use Doctrine\ORM\Mapping as ORM;
use Tchoulom\ViewCounterBundle\Entity\ViewCounter as BaseViewCounter;

/**
 * @ORM\Table(name="view_counter")
 * @ORM\Entity(repositoryClass=ViewCounterRepository::class)
 */
class ViewCounter extends BaseViewCounter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="viewCounters")
     */
    private $product;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $type;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(?int $type): self
    {
        $this->type = $type;

        return $this;
    }

}
