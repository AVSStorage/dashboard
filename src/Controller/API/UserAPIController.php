<?php


namespace App\Controller\API;

use App\Entity\User;
use App\Form\RegistrationLegalFormType;
use App\Repository\UserIndividualRepository;
use App\Repository\UserLegalRepository;
use App\Repository\UserRepository;
use App\Serializer\Normalizer\FormNormalizer;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Forms;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\UserIndividual;
use App\Form\RegistrationIndividualFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserAPIController extends AbstractController
{
    private $serializer;
    public function __construct()
    {
        $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/api/user/email/check", name="api_user_email_check", format="json", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function index(Request $request, UserRepository $userRepository, UserLegalRepository $userLegalRepository)
    {

        $form = $this->createFormBuilder(null,['csrf_protection' => false])
            ->add('email', EmailType::class,[
                'constraints' => new Email([
                    'message' => 'Неправильный формат'
                ])
            ])
            ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $userRepository->findOneBy(['email' => $form->get('email')->getData()]);

            if ($user) {
                return new JsonResponse(['message' => 'Данная почта уже существует', 'email' => $form->get('email')->getData()], Response::HTTP_NOT_ACCEPTABLE);
            }
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            return new JsonResponse(['message' => 'Невалидные данные'], Response::HTTP_NOT_ACCEPTABLE);
        }

        $view = $this->serializer->serialize( ['message' => 'OK'], 'json');
        return JsonResponse::fromJsonString($view);
    }

    /**
     * @Route("/api/user/ul/form/email", name="api_user_get_ul_email", format="json", methods={"GET"}, condition="request.isXmlHttpRequest()")
     */
    public function getUlEmail(Request $request, UserRepository $userRepository)
    {

        $ulForm = $this->createForm(RegistrationLegalFormType::class);
        $fields = $ulForm->createView()->children;
        $normalizers = new FormNormalizer(new ObjectNormalizer());
        $data = $normalizers->normalize($ulForm,'json', ['neededFieldsArray' =>  [ 'id','full_name', 'label',  'label_attr' , 'required', 'attr'] , 'fields' => $fields]);
        $view = $this->serializer->serialize( [ 'email' => $data['email']], 'json');
        return JsonResponse::fromJsonString($view);
    }
}