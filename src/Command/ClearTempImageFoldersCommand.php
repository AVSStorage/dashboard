<?php

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class ClearTempImageFoldersCommand extends Command
{
    protected static $defaultName = 'app:clear-temp-image-folders';

    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var ParameterBagInterface
     */
    private $parameters;


    const IMAGE_DIR = '/public/';
    const TEMP_DIR = 'temp';
    /**
     * @var LoggerInterface
     */
    private $logger;


    public function __construct(string $name = null, Filesystem $filesystem, ParameterBagInterface $parameters, LoggerInterface $logger)
    {
        parent::__construct($name);
        $this->filesystem = $filesystem;
        $this->parameters = $parameters;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this
            ->setDescription('Clear temp image folders in public/files/user_id/temp ');
    }

    private function deleteFile($file) {
        if ($this->filesystem->exists($file)) {
            $this->filesystem->remove($file);
        }
    }

    private function getProductImageDirs($finder) {
        return $finder->directories()->in($this->parameters->get('kernel.project_dir') . self::IMAGE_DIR . $this->parameters->get('image_directory'))->depth('> 0');
    }

    private function loopDirs() {

        $finder = new Finder();
        $dirs = $this->getProductImageDirs($finder);


        foreach ($dirs as $dir) {
            if ($dir->getFilename() === self::TEMP_DIR) {
                $dirName = $dir->getPathname();
                $this->deleteFile($dirName);
                break;
            }

        }



        return true;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);


        $this->loopDirs();


        $this->logger->info('Temp product image dir/dirs is/are cleaned ');

        $io->success('Temp product image dirs are cleaned ');

        return 0;
    }
}
