import Vue from 'vue'
import Vuex from 'vuex'
import FormDataModel from './module/formDataModel'
import CategoryModel from "./module/categoryModel";
import ImageModel from "./module/ImageModel";
Vue.use(Vuex)

export default new Vuex.Store({
    state: {
    },
    mutations: {
    },
    actions: {
    },
    modules: {
        FormData: FormDataModel,
        CategoryModel: CategoryModel,
        ImageModel: ImageModel
    }
})
