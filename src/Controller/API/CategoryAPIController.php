<?php

namespace App\Controller\API;

use App\Entity\Category;
use App\Entity\CategoryStatuses;
use App\Entity\Product;
use App\Form\ProductFormType;
use App\Repository\CategoryRepository;
use App\Repository\FilterRepository;
use App\Repository\FilterValuesRepository;
use App\Serializer\Normalizer\CollectionNormalizer;
use App\Serializer\Normalizer\FilterNormalizer;
use App\Serializer\Normalizer\FormNormalizer;
use App\Service\CategoryService;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CategoryAPIController extends AbstractController
{
    private $serializer;
    public function __construct()
    {
        $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/category/add-form/get", name="category_api_form", methods={"GET"})
     */
    public function index(CategoryRepository $categoryRepository)
    {
        $product = new Product();
        $form = $this->createForm(ProductFormType::class, $product);
        $fields = $form->createView()->children;

        $normalizers = new FormNormalizer(new ObjectNormalizer());
        $data = $normalizers->normalize($form,'json', ['neededFieldsArray' =>  [ 'id','full_name', 'label',  'label_attr' , 'required', 'attr'] , 'fields' => $fields]);
        $view = $this->serializer->serialize(array_merge($data['category'], ['choices' => $categoryRepository->findCategoriesByLvl(0),'form_name' => $form->createView()->vars['name'], 'lvl' => 0]), 'json');
        return JsonResponse::fromJsonString($view);
    }


    /**
     * @Route("/filter/models/get", name="filter_api_get_car_models", methods={"POST"})
     */
    public function getFilters(Request $request, FilterValuesRepository $filterValuesRepository) {

        $value_id =  (int)$request->get('value_id');
        $models = $filterValuesRepository->findBy(['filter' => 2,'value_id' => $value_id]);
        $collectionNormalizer = new CollectionNormalizer(new ObjectNormalizer());
        $models = $collectionNormalizer->normalize($models);

        return JsonResponse::fromJsonString($this->serializer->serialize($models, 'json'));
    }




    /**
     * @Route("/category/get", name="category_api_get", methods={"POST"})
     */
    public function getCategoriesAndTypes(Request $request, CategoryRepository $categoryRepository, CategoryService $categoryService, FilterRepository $filterRepository){

        $lvl = (int)$request->get('lvl');
        $categoryId = (int)$request->get('id');

        $category = $categoryRepository->findOneBy(['id' => $categoryId, 'status' => CategoryStatuses::STATUS_ACTIVE]);


        $collectionNormalizer = new CollectionNormalizer(new ObjectNormalizer());
        $filtersNormalizer = new FilterNormalizer(new ObjectNormalizer());


        $filters = [];

        if(!$category) {
            return JsonResponse::fromJsonString($this->serializer->serialize([
                'choices' => $categoryRepository->getRootNodesQueryBuilder()->orderBy('node.sort_order','asc')->getQuery()->getArrayResult(),
                'filters' => [],
                'lvl' => 0 ],
                'json'));
        }

        if ($category->getLvl() > 0) {
            $filters = $categoryService->getParentFilters($category);
        }




//        if (!$category->getFilters()->isEmpty()) {
//
//            array_merge($filters, $category->getFilters()->getValues());
//        }



        $filters = $filtersNormalizer->normalize($filters);

        if ($categoryRepository->childCount($category) > 0) {

            $value = $category->getChildren()->getValues();
            usort($value, function ($item1, $item2) {
                if ($item1->sort_order == $item2->sort_order) return 0;
                return $item1->sort_order > $item2->sort_order ? -1 : 1;
            });

            $categories = $collectionNormalizer->normalize($value);


            if ($lvl < 2 && !empty($categories)) {
                $type = $collectionNormalizer->normalize($category->getProductTypes()->getValues());
                $view = $this->serializer->serialize(['choices' => $categories, 'types' => $type, 'filters' => $filters, 'lvl' => $category->getLvl() + 1 ], 'json');
            } else {
                $view = $this->serializer->serialize(['choices' => $categories, 'filters' => $filters, 'lvl' => $category->getLvl() + 1 ], 'json');
            }
        } else {
            $type = [];
            if ($category->getProductTypes()->getValues()) {
                $type = $collectionNormalizer->normalize($category->getProductTypes()->getValues());
            }
            $view = $this->serializer->serialize(['parent' => false,  'filters' => $filters, 'types' => $type, 'lvl' => $category->getLvl()], 'json');
        }

        return JsonResponse::fromJsonString($view);

    }

}
