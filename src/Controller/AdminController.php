<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\CategoryStatuses;
use App\Entity\Filter;
use App\Form\CategoryType;
use App\Form\FilterFormType;
use App\Repository\CategoryRepository;
use App\Repository\CategoryStatusesRepository;
use App\Repository\FilterRepository;
use App\Repository\ProductRepository;
use App\Service\CategoryService;
use App\Service\ProductService;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     * @IsGranted("ROLE_ADMIN")
     *
     */
    public function index(Request $request, ProductRepository $productRepository, PaginatorInterface $paginator)
    {
        $query = $productRepository->getPaginateItemsQuery();

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );



        $pagination->setTemplate('@KnpPaginator/Pagination/twitter_bootstrap_v4_pagination.html.twig');

        $pagination->setSortableTemplate('@KnpPaginator/Pagination/twitter_bootstrap_v3_sortable_link.html.twig');
        return $this->render('admin/index.html.twig', [
            'products' => $pagination
        ]);
    }

    /**
     * @Route("/admin/categories", name="admin_categories")
     * @IsGranted("ROLE_ADMIN")
     */
    public function categories(Request $request, CategoryRepository $categoryRepository, CategoryStatusesRepository $categoryStatusesRepository,  PaginatorInterface $paginator, EntityManagerInterface $entityManager)
    {



        $categories = $categoryRepository->findBy([],['status' => 'ASC']);
        return $this->render('admin/categories.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/admin/filters/add", name="admin_filters_add")
     * @IsGranted("ROLE_ADMIN")
     */
    public function addFilters(Request $request, CategoryStatusesRepository $categoryStatusesRepository, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager)
    {
        $filter = new Filter();
        $form = $this->createForm(FilterFormType::class, $filter, ['has_category' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){

            $category = $categoryRepository->findOneBy(['id' => $form->get('category')->getData()]);
            $values = $form->get('values')->getData();
            $name = $form->get('name')->getData();
            $filter->setName($name);
            $filter->setType(Filter::SELECT_FILTER);
            $filter->setValues($values);
            $filter->addCategory($category);
            $entityManager->persist($filter);
            $entityManager->flush();
        }



        $choices = [];
        $i = 0;
        while ($i < 2) {
            $choices[] = $categoryRepository->childrenQueryBuilder()->where('node.lvl = '. $i)->getQuery()->getArrayResult();
            $i++;
        }



        return $this->render('admin/filters-add.html.twig', [
            'form' => $form->createView(),
            'action' => 'add',
            'form_name' => $form->createView()->vars['name'],
            'categoryData' => ['choices' => $choices[0]]
        ]);
    }

    /**
     * @Route("/admin/filters/{id}/edit", name="admin_filters_edit")
     * @IsGranted("ROLE_ADMIN")
     */
    public function editFilter(Request $request, FilterRepository $filterRepository, CategoryStatusesRepository $categoryStatusesRepository, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager)
    {
        $filter = $filterRepository->findOneBy(['id' => $request->attributes->get('id')]);
        $form = $this->createForm(FilterFormType::class,$filter);
        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()){
            $values = $form->get('values')->getData();
            $name = $form->get('name')->getData();
            $filter->setValues($values);
            $filter->setName($name);
            $entityManager->persist($filter);
            $entityManager->flush();
        }

        $filterValues = $filter->getValues();
        $values = [];
        if ($filterValues) {
            foreach ($filterValues as $filterValue) {
                $value['value'] = $filterValue;
                $value['show'] = true;
                $values[] = $value;
            }
        }



        return $this->render('admin/filters/edit.html.twig', [
            'form' => $form->createView(),
            'action' => 'edit',
            'categories' => $filter->getCategory(),
            'values' => $values,
            'form_name' => $form->createView()->vars['name'],
        ]);
    }

    /**
     * @Route("/admin/categories/add", name="admin_categories_add")
     * @IsGranted("ROLE_ADMIN")
     */
    public function addCategories(Request $request, CategoryStatusesRepository $categoryStatusesRepository, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

       $choices = [];
       $i = 0;
       while ($i < 2) {
           $choices[] = $categoryRepository->childrenQueryBuilder()->where('node.lvl = '. $i)->getQuery()->getArrayResult();
           $i++;
       }


       if ($form->isSubmitted() && $form->isValid()){

           $names = $form->get('name')->getData();
           $filters = $form->get('filters')->getData();

           $parent = $categoryRepository->findOneBy(['id' => $form->get('category')->getData()]);


           foreach ($names as $name) {
               $category = new Category();
               if ($filters->count() > 0) {
                   foreach ($filters as $filter) {
                       $category->addFilter($filter);
                       $entityManager->persist($category);
                   }
               }

               $category->setStatus($categoryStatusesRepository->findOneBy(['id' => CategoryStatuses::STATUS_ACTIVE]));
               $category->setName($name);
               $category->setParent($parent);
               $entityManager->persist($category);

               $entityManager->flush();

           }





           $this->addFlash('success','Категория была успешно добавлена');
       }


        return $this->render('admin/categories-add.html.twig', [
           'form' => $form->createView(),
            'action' => 'add',
            'form_name' => $form->createView()->vars['name'],
            'categoryData' => ['choices' => $choices[0]]
        ]);
    }



    /**
     * @Route("/admin/filters", name="admin_filters")
     * @IsGranted("ROLE_ADMIN")
     */
    public function filters(Request $request, FilterRepository $filterRepository){
        $filters = $filterRepository->findAll();

        return $this->render('admin/filters.html.twig', [
            'filters' => $filters
        ]);

    }

    /**
     * @Route("admin/category/{id}/active", name="admin_category_active")
     * @IsGranted("ROLE_ADMIN")
     */
    public function setCategoryActive(Request $request, CategoryRepository $categoryRepository, CategoryStatusesRepository $categoryStatusesRepository, EntityManagerInterface $entityManager) {
        $category = $categoryRepository->findOneBy(['id' => $request->attributes->get('id')]);
        $category->setStatus($categoryStatusesRepository->findOneBy(['id' => CategoryStatuses::STATUS_ACTIVE]));
        $entityManager->persist($category);
        $entityManager->flush();
        return $this->redirectToRoute('admin_categories');
    }


    /**
     * @Route("admin/category/{id}/edit", name="admin_category_edit")
     * @IsGranted("ROLE_ADMIN")
     */
    public function editCategory(Request $request, EntityManagerInterface $entityManager, ProductService $productService, CategoryRepository $categoryRepository, CategoryService $categoryService, ProductRepository $productRepository)
    {

        $product = $productRepository->findOneBy([]);

        $category = $categoryRepository->findOneBy(['id' => $request->attributes->get('id')]);
        $form = $this->createForm(CategoryType::class, $category,['lvl' => $category->getLvl(), 'show_parent' => true]);

        $form->handleRequest($request);
        $data = $categoryService->getEditCategoriesData($product);



        if ($form->isSubmitted() && $form->isValid()){
            $parent = $categoryRepository->findOneBy(['id' => $form->get('parent_category')->getData()]);
            $sort_order = $categoryRepository->findOneBy(['sort_order' => $form->get('sort_order')->getData()]);
            if ($sort_order) {
                $form->addError(new FormError('Данный порядок сортировки уже существует. Укажите пожалуйста другой'));
            } else {
                $category->setSortOrder((float)$form->get('sort_order')->getData());
            }
            if ($parent) {
                $category->setParent($parent);
            } else {
                $form->addError(new FormError('Категории с заданным ID не существует'));
            }

            $filters = $form->get('filters')->getData();
            foreach ($filters as $filter) {
                $category->addFilter($filter);
            }

            $entityManager->persist($category);
            $entityManager->flush();
        }




        if (!$form->isSubmitted()) {

            $form->get('filters')->setData(new ArrayCollection($category->getFilters()->getValues()));
            if ($category->getParent()) {
                $form->get('parent_category')->setData($category->getParent()->getId());
            }
        }



        return $this->render('category/edit.html.twig', [
            'action' => 'edit',
            'form' => $form->createView(),
            'category_lvl' => $category->getLvl(),
            'form_name' => $form->createView()->vars['name'],
            'categoryData' => $data
        ]);
    }
}
