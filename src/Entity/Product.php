<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Tchoulom\ViewCounterBundle\Model\ViewCountable;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Product implements ViewCountable
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=ProductType::class, mappedBy="product")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     */
    private $address;


    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="array")
     */
    private $photos = [];

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;


    /**
     * @ORM\OneToMany(targetEntity=ProductFilter::class, mappedBy="product", orphanRemoval=true)
     */
    public $productFilters;


    private $imagePath;

    private $imageThumbnailPath;

    /**
     * @ORM\ManyToOne(targetEntity=ProductStatuses::class, inversedBy="products")
     * @ORM\JoinColumn(nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $reason;

    /**
     * @ORM\OneToMany(targetEntity=ViewCounter::class, mappedBy="product")
     */
    private $viewCounters;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $views;

    /**
     * @ORM\OneToMany(targetEntity=ClickCounter::class, mappedBy="product")
     */
    private $clickCounters;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $clicks;





    public function __construct()
    {
        $this->category_name = new ArrayCollection();
        $this->category = new ArrayCollection();
        $this->type = new ArrayCollection();
        $this->filters = new ArrayCollection();
        $this->productFilters = new ArrayCollection();
        $this->viewCounters = new ArrayCollection();
        $this->clickCounters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getImagePath($image_directory) {
        return $image_directory . $this->getUser()->id. '/' . $this->getId();
    }

    public function getImageThumbnailPath($image_directory ) {
        return $image_directory . $this->getUser()->id . '/' . $this->getId() . '/thumbnails';
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhotos(): ?array
    {
        return $this->photos;
    }

    public function getPhotosUrl($image_directory): ?array
    {
        $photoUrls = [];
        $photos = $this->getPhotos();
        foreach ($photos as $photo) {
            $photoUrls[] = $image_directory . $this->getUser()->id . '/' . $this->getId() . '/' . $photo;
        }

        return $photoUrls;
    }

    public function setPhotos(array $photos): self
    {
        $this->photos = $photos;

        return $this;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategory()
    {
        return $this->category;
    }

    public function addCategory(Category $category): self
    {

        if (!$this->category->contains($category)) {
            $this->category[] = $category;
            $category->setProduct($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->category->contains($category)) {
            $this->category->removeElement($category);
            // set the owning side to null (unless already changed)
            if ($category->getProduct() === $this) {
                $category->setProduct(null);
            }
        }

        return $this;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }


    /**
     * @return Collection|ProductFilter[]
     */
    public function getProductFilters(): Collection
    {
        return $this->productFilters;
    }

    public function addProductFilter(ProductFilter $productFilter): self
    {
        if (!$this->productFilters->contains($productFilter)) {
            $this->productFilters[] = $productFilter;
            $productFilter->setProduct($this);
        }

        return $this;
    }

    public function removeProductFilter(ProductFilter $productFilter): self
    {
        if ($this->productFilters->contains($productFilter)) {
            $this->productFilters->removeElement($productFilter);
            // set the owning side to null (unless already changed)
            if ($productFilter->getProduct() === $this) {
                $productFilter->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductType[]
     */
    public function getType(): Collection
    {
        return $this->type;
    }


    public function addType(ProductType $productType): self
    {
        if (!$this->type->contains($productType)) {
            $this->type[] = $productType;
            $productType->addProduct($this);
        }

        return $this;
    }

    public function removeProductType(ProductType $productType): self
    {
        if ($this->type->contains($productType)) {
            $this->type->removeElement($productType);
            $productType->removeProduct($this);
        }

        return $this;
    }

    public function getStatus(): ?ProductStatuses
    {
        return $this->status;
    }

    public function setStatus(?ProductStatuses $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setReason(?string $reason): self
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * @return Collection|ViewCounter[]
     */
    public function getViewCounters(): Collection
    {
        return $this->viewCounters;
    }

    public function addViewCounter(ViewCounter $viewCounter): self
    {
        if (!$this->viewCounters->contains($viewCounter)) {
            $this->viewCounters[] = $viewCounter;
            $viewCounter->setProduct($this);
        }

        return $this;
    }

    public function removeViewCounter(ViewCounter $viewCounter): self
    {
        if ($this->viewCounters->contains($viewCounter)) {
            $this->viewCounters->removeElement($viewCounter);
            // set the owning side to null (unless already changed)
            if ($viewCounter->getProduct() === $this) {
                $viewCounter->setProduct(null);
            }
        }

        return $this;
    }

    public function getViews(): ?int
    {
        return $this->views;
    }

    /**
     * Sets $views
     *
     * @param integer $views
     *
     * @return $this
     */
    public function setViews($views)
    {
        $this->views = $views;

        return $this;
    }

    /**
     * @return Collection|ClickCounter[]
     */
    public function getClickCounters(): Collection
    {
        return $this->clickCounters;
    }

    public function addClickCounter(ClickCounter $clickCounter): self
    {
        if (!$this->clickCounters->contains($clickCounter)) {
            $this->clickCounters[] = $clickCounter;
            $clickCounter->setProduct($this);
        }

        return $this;
    }

    public function removeClickCounter(ClickCounter $clickCounter): self
    {
        if ($this->clickCounters->contains($clickCounter)) {
            $this->clickCounters->removeElement($clickCounter);
            // set the owning side to null (unless already changed)
            if ($clickCounter->getProduct() === $this) {
                $clickCounter->setProduct(null);
            }
        }

        return $this;
    }

    public function getClicks(): ?int
    {
        return $this->clicks;
    }

    public function setClicks(?int $clicks): self
    {
        $this->clicks = $clicks;

        return $this;
    }






}
