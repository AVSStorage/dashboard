<?php

namespace App\Repository;

use App\Entity\ProductFilterView;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductFilterView|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductFilterView|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductFilterView[]    findAll()
 * @method ProductFilterView[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductFilterViewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductFilterView::class);
    }

    // /**
    //  * @return ProductFilterView[] Returns an array of ProductFilterView objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductFilterView
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
