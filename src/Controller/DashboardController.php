<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Filter;
use App\Entity\Product;
use App\Entity\ProductFilter;
use App\Entity\ProductType;
use App\Entity\User;
use App\Entity\UserIndividual;
use App\Event\NewProductEvent;
use App\EventSubscriber\ProductSubscriber;
use App\Form\ProductFormType;
use App\Form\RegistrationIndividualFormType;
use App\Form\RegistrationLegalFormType;
use App\Repository\CategoryRepository;
use App\Repository\FilterRepository;
use App\Repository\FilterValuesRepository;
use App\Repository\ProductFilterRepository;
use App\Repository\ProductRepository;
use App\Repository\ProductTypeRepository;
use App\Repository\UserIndividualRepository;
use App\Repository\UserLegalRepository;
use App\Repository\UserRepository;
use App\Serializer\Normalizer\StatisticsNormilizerNormalizer;
use App\Service\ProductService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;
use Tchoulom\ViewCounterBundle\Finder\StatsFinder;

class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     * @IsGranted("ROLE_USER")
     *
     */
    public function index(CategoryRepository $categoryRepository, FilterRepository $filterRepository)
    {

        if ($this->isGranted('ROLE_ADMIN')) {
            return new RedirectResponse('/admin');
        }

        return $this->render('dashboard/index.html.twig', [
            'controller_name' => 'DashboardController',
        ]);
    }

    /**
     * @Route("/dashboard/messages", name="dashboard_messages")
     * @IsGranted("ROLE_USER")
     *
     */
    public function messages(Request $request)
    {
        return $this->render('dashboard/messages.html.twig', [

        ]);
    }

    /**
     * @Route("/dashboard/pay", name="dashboard_payment")
     * @IsGranted("ROLE_USER")
     *
     */
    public function pay(Request $request)
    {
        return $this->render('dashboard/payment.html.twig', [

        ]);
    }

    /**
     * @Route("/dashboard/statistics", name="dashboard_statistics")
     * @IsGranted("ROLE_USER")
     *
     */
    public function statistics(Request $request,StatisticsNormilizerNormalizer $statisticsNormilizerNormalizer, StatsFinder $statsFinder, Security $security, ProductRepository $productRepository){
        $products = $productRepository->findBy(['user' => $security->getUser()->id]);
        $weekNumber = (new \DateTime('now'))->format('W');
        $monthNumber = (new \DateTime('now'))->format('m');
        $year = (new \DateTime('now'))->format('Y');
        $day = (new \DateTime('now'))->format('l');

        $statistics = [];
        foreach ($products as $product) {
            $statisticsData = $statsFinder->getDailyStats($product, $year, $monthNumber, $weekNumber);
            if ($statisticsData) {
                $statistics[$product->getId()] = $statisticsNormilizerNormalizer->normalize($statisticsData, null, ['day' => $day]);
            }
       }



        return $this->render('dashboard/statistics.html.twig', [
            'products' => $products,
            'statistics' => $statistics
        ]);
    }

    /**
     * @Route("/dashboard/subscribe", name="dashboard_subscribe")
     * @IsGranted("ROLE_USER")
     *
     */
    public function subscribe(Request $request)
    {
        return $this->render('dashboard/subscribe.html.twig', [

        ]);
    }

    /**
     * @Route("/dashboard/add", name="dashboard_add")
     * @IsGranted("ROLE_USER")
     *
     */
    public function add(Request $request,ProductService $productService,
                        UserPasswordEncoderInterface $userPasswordEncoder,
                        EntityManagerInterface $entityManager,
                        FilterRepository $filterRepository,
                        SluggerInterface $slugger,
                        CategoryRepository $categoryRepository,
                        Filesystem $filesystem, UserRepository $userRepository,
                        FilterValuesRepository $filterValuesRepository,
                        ProductFilterRepository $productFilterRepository,
                        ParameterBagInterface $parameter,
                        Security $security,
                        EventDispatcherInterface $dispatcher,
                        ProductSubscriber $productSubscriber
    )
    {

        if ($this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('admin');
        }


        $product = new Product();
        $form = $this->createForm(ProductFormType::class,$product);
        $form->handleRequest($request);



        if (!$form->isSubmitted()) {
            $productService->clearTempDir();
        }
        // Добавление своих категорий






        if ($form->isSubmitted() && $form->isValid()){

            $newCategory = null;
            if ($request->request->get('new_category_parent')){
               $newCategory['parent'] = $request->request->get('new_category_parent');
               $newCategory['name'] = $request->request->get('new_category_name');
            }

            $allFormValues = $request->request->get('product_form');

            $filter = $allFormValues['filter'];



            $models = null;
            if ((int)$allFormValues['category'] === 136) {
                $models = $request->request->get('model');
            }


            $productService->setNewProduct($form, $product, 'new', $filter,  $newCategory, $models);
            $this->addFlash('success','Ваше объявление было передано на модерацию!');

//            $dispatcher->addListener('product.new', array($productSubscriber, 'onProductNew'));
            $url = $this->generateUrl('product',['id' =>  $product->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
            // dispatch
            $dispatcher->dispatch( new NewProductEvent($product, $url), NewProductEvent::NAME);


            return $this->redirect($request->getUri());
        }



        return $this->render('dashboard/add.html.twig', [
            'controller_name' => 'ProductController',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/dashboard/products", name="dashboard_products")
     * @IsGranted("ROLE_USER")
     *
     */
    public function products(ProductRepository $productRepository) {
        $products = $productRepository->findBy(['user' => $this->getUser()->id ], ['createdAt' => 'desc']);

        return $this->render('dashboard/products.html.twig', [
            'products' => $products
        ]);

    }


    /**
     * @Route("/dashboard/user", name="dashboard_user")
     * @IsGranted("ROLE_USER")
     *
     */
    public function userData(Request $request,Security $security, UserPasswordEncoderInterface $encoder, EntityManagerInterface $entityManager, UserRepository $userRepository, UserLegalRepository $userLegalRepository,  UserIndividualRepository $userIndividualRepository) {
        $user =$userRepository->findOneBy(['id' => $security->getUser()]);
        $userIndividual = $user->getUserIndividual();

        if ($this->isGranted('ROLE_INDIVIDUAL')) {


            $form = $this->createForm(RegistrationIndividualFormType::class, $userIndividual, ['edit_form' => true]);
            $form->handleRequest($request);
            if (!$form->isSubmitted()) {
                $form->get('email')->setData($user->getEmail());
                $form->get('phone')->setData($user->getPhone());
            }

            if ($form->isSubmitted() && $form->isValid()) {

                if ($form->get('password')->getData()) {
                    $user->setPassword($encoder->encodePassword($user, $form->get('password')->getData()));
                }
                $entityManager->persist($user);
                $entityManager->persist($userIndividual);
                $entityManager->flush();
                $this->addFlash('success','Данные были успешно изменены');
            }

            return $this->render('dashboard/user/person.html.twig', [
                'form' => $form->createView()
            ]);
        } else {
            $userLegal = $user->getUserLegal();

            $form = $this->createForm(RegistrationLegalFormType::class, $userLegal, ['edit_form' => true]);
            $form->handleRequest($request);
            if (!$form->isSubmitted()) {
                $form->get('email')->setData($user->getEmail());
                $form->get('phone')->setData($user->getPhone());
                if ($userIndividual) {
                    $form->get('name')->setData($userIndividual->getName());
                }
            }

            if ($form->isSubmitted() && $form->isValid()) {
                $userIndividual->setName($form->get('name')->getData());
                if ($form->get('password')->getData()) {
                    $user->setPassword($encoder->encodePassword($user, $form->get('password')->getData()));
                }
                $entityManager->persist($user);
                $entityManager->persist($userIndividual);
                $entityManager->persist($userLegal);
                $entityManager->flush();
                $this->addFlash('success','Данные были успешно изменены');
            }

            return $this->render('dashboard/user/person.html.twig', [
                'form' => $form->createView()
            ]);
        }

    }


}
