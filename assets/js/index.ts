import '../css/app.css';
import {MobileInputComponent,LightboxComponent, EditPersonFormComponent, InputAddComponent,  ImageInputComponent, InputWithSeparatorComponent, ChartComponent, SliderComponent, SubmitButtonComponent, LoginPersonComponent, ModalComponent, InfoMessageComponent, InputComponent, CategoriesComponent, ModalMessage, PasswordInputComponent, InfoModalMessageWithStateComponent} from "./app/index";

if (document.getElementById('inputAddComponent')) {
    InputAddComponent.$mount('#inputAddComponent');
}

if (document.getElementById('lightbox')) {
    LightboxComponent.$mount('#lightbox');
}

if (document.getElementById('imageInputComponent')) {
    ImageInputComponent.$mount('#imageInputComponent');
}

if (document.getElementById('inputWithSeparator')) {
    InputWithSeparatorComponent.$mount('#inputWithSeparator');
}

if (document.getElementById('chartComponent')) {
    ChartComponent.$mount('#chartComponent');
}


if (document.getElementById('mobileComponent')) {
    MobileInputComponent.$mount('#mobileComponent');
}

if (document.getElementById('sliderComponent')) {
    SliderComponent.$mount('#sliderComponent');
}

if (document.getElementById('loginPersonComponent')) {
    LoginPersonComponent.$mount('#loginPersonComponent');
}

if (document.getElementById('modalComponent')) {
    ModalComponent.$mount('#modalComponent');
}

if (document.getElementById('infoMessageComponent')) {
    InfoMessageComponent.$mount('#infoMessageComponent');
}

if (document.getElementById('inputComponent')) {
    InputComponent.$mount('#inputComponent');
}

if (document.getElementById('infoModalMessage')) {
    ModalMessage.$mount('#infoModalMessage');
}

if (document.getElementById('passwordInput')) {
    PasswordInputComponent.$mount('#passwordInput');
}

if (document.getElementById('modalWithState')) {
    InfoModalMessageWithStateComponent.$mount('#modalWithState');
}

if (document.getElementById('ajaxSubmitButton')) {
    SubmitButtonComponent.$mount('#ajaxSubmitButton');
}

if (document.getElementById('categoriesComponent')) {
    CategoriesComponent.$mount('#categoriesComponent');
}

if (document.getElementById('editPersonForm')) {
    EditPersonFormComponent.$mount('#editPersonForm')
}