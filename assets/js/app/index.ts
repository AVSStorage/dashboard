import Vue from 'vue'
import MobileInput from "./components/MobileInput.vue";
import Slider from "./components/Slider.vue";
import Form from "./components/Form.vue";
import store from './store/index';
import InfoMessage from "./components/InfoMessage.vue";
import InputWithState from "./components/InputWithState.vue";
import InfoModalMessage from "./components/InfoModalMessage.vue";
import PasswordInput from "./components/PasswordInput.vue";
import InfoModalMessageWithState from "./components/InfoModalMessageWithState.vue";
import SubmitButton from "./components/SubmitButton.vue";
import ResetPasswordHiddenModal from "./components/ResetPasswordHiddenModal.vue";
import Categories from "./components/Categories.vue";
import Chart from "./components/Chart.vue";
import InputWithSeparator from "./components/InputWithSeparator.vue";
import ImageInput from "./components/ImageInput.vue";
import LightBoxComponent from "./components/LightBoxComponent.vue";
import InputCategoriesComponent from "./components/InputComponent.vue";
import EditPersonForm from "./components/EditPersonForm.vue";
Vue.config.productionTip = false;
Vue.config.devtools = true;

export const ImageInputComponent = new Vue({
    // render: (h) => h(MobileInput),
    store,
    components: {
        ImageInput
    }
});

export const InputAddComponent = new Vue({
    // render: (h) => h(MobileInput),

    components: {
        InputAdd: InputCategoriesComponent
    }
});

export const LightboxComponent = new Vue({
    // render: (h) => h(MobileInput),

    components: {
        LightBoxComponent
    }
});

export const InputWithSeparatorComponent = new Vue({
    // render: (h) => h(MobileInput),
    components: {
        InputWithSeparator
    }
});

export const CategoriesComponent = new Vue({
    // render: (h) => h(MobileInput),
    store,
    components: {
        Categories
    }
});

export const ChartComponent = new Vue({
    // render: (h) => h(MobileInput),
    components: {
       Chart
    }
});

export const SubmitButtonComponent = new Vue({
    // render: (h) => h(MobileInput),
    store,
    components: {
        AjaxSubmitButton: SubmitButton
    }
});


export const MobileInputComponent = new Vue({
    // render: (h) => h(MobileInput),
    components: {
        MobileInput
    }
});

export const ModalMessage = new Vue({
    // render: (h) => h(MobileInput),
    components: {
        InfoModalMessage
    }
});


export const SliderComponent = new Vue({
    components: {
        Slider
    }
});


export const LoginPersonComponent = new Vue({
    store,
    components: {
        FormComponent: Form
    }
});

export const ModalComponent = new Vue({
    store,
    components: {
        ResetPasswordHiddenModal,
        PasswordInput
    }
});

export const InfoMessageComponent = new Vue({
    components: {
        InfoMessage
    }
});

export const InputComponent = new Vue({
    store,
    components: {
        InputComponent: InputWithState
    }
});


export const PasswordInputComponent = new Vue({
    components: {
        PasswordInput
    }
});

export const InfoModalMessageWithStateComponent = new Vue({
    store,
    components: {
        InfoModalMessageWithState
    }
});

export const EditPersonFormComponent = new Vue({
    store,
    components: {
        EditPersonForm
    }
});