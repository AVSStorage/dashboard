<?php

namespace App\Entity;

use App\Repository\ClickCounterRepository;
use Doctrine\ORM\Mapping as ORM;
use Tchoulom\ViewCounterBundle\Entity\ViewCounter as BaseViewCounter;

/**
 * @ORM\Entity(repositoryClass=ClickCounterRepository::class)
 */
class ClickCounter extends BaseViewCounter
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="clickCounters")
     */
    private $product;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
