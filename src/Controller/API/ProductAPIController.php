<?php


namespace App\Controller\API;


use App\Form\ImageFormType;
use App\Repository\UserLegalRepository;
use App\Repository\UserRepository;
use App\Service\FileUploadService;
use App\Service\ProductImageUploadService;
use App\Service\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Routing\Annotation\Route;


class ProductAPIController  extends AbstractController
{

    private $serializer;
    public function __construct()
    {
        $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/api/product/image/upload", name="api_product_image_upload", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function addPhoto(Request $request, ProductImageUploadService $productImageUploadService, ProductService $productService)
    {

        $productId = $request->request->get('productId');
        $form = $this->createForm(ImageFormType::class);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()){

            $image  = $form->get('image')->getData();
            $productService->createImageDir();

            try {
                $photos = $productImageUploadService->setProductPhoto($image, $productId);
                return new JsonResponse(['uploaded' => true, 'full_path' => $photos['full_path'] , 'original' => $photos['image']], Response::HTTP_OK);
            } catch (FileException $exception) {
                return new JsonResponse(['uploaded' => false ], Response::HTTP_BAD_REQUEST);
            }

        } else {
            $errorMessage = $form->get('image')->getErrors()->current()->getMessage();
            return new JsonResponse(['message' => $errorMessage], Response::HTTP_BAD_REQUEST);
        }


    }


    /**
     * @Route("/api/product/image/delete", name="api_product_image_delete", methods={"POST"}, condition="request.isXmlHttpRequest()")
     */
    public function deletePhoto(Request $request, ProductImageUploadService $productImageUploadService)
    {
        $image  = $request->get('image');
        $action = $request->get('action');
        $productId = $request->get('productId');

        try {
            $productImageUploadService->deleteProductPhoto($image, $action, $productId);
            return new JsonResponse(['deleted' => true ], Response::HTTP_OK);
        } catch (IOException $exception) {
            return new JsonResponse(['deleted' => false ], Response::HTTP_BAD_REQUEST);
        }
    }

}