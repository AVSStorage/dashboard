<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;



/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @UniqueEntity(fields={"email"}, message="Данный email уже существует")
 */
class User implements UserInterface
{

    const ROLE_INDIVIDUAL = "ROLE_INDIVIDUAL";
    const ROLE_LEGAL = "ROLE_LEGAL";
    const ROLE_ADMIN = "ROLE_ADMIN";


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Assert\NotBlank()
     * @Assert\Unique
     *
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;


    /**
     * @ORM\Column(type="string")
     * @Assert\Regex(pattern="/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/", message="Неправильный формат")
     */
    private $phone;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="user", orphanRemoval=true)
     */
    private $products;

    private $businessRole;

    /**
     * @ORM\OneToOne(targetEntity=UserIndividual::class, inversedBy="user_link", cascade={"persist", "remove"})
     */
    public $user_individual;


    /**
     * @ORM\OneToOne(targetEntity=UserLegal::class, inversedBy="user_link", cascade={"persist", "remove"})
     */
    public $user_legal;


    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function getBusinessRole() {
        $translatedRoles = [
            self::ROLE_INDIVIDUAL => 'Физ.лицо',
            self::ROLE_LEGAL => 'Юр.лицо'
        ];
        if (!empty($this->roles)) {
           foreach ($this->roles as $role) {
               if (in_array($role, array_keys($translatedRoles)))
               return $translatedRoles[$role];
           }
        }

        return [];
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setUserId($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getUserId() === $this) {
                $product->setUserId(null);
            }
        }

        return $this;
    }

    public function getInfo(): ?AllUsers
    {
        return $this->info;
    }

    public function setInfo(AllUsers $info): self
    {
        $this->info = $info;

        return $this;
    }

    public function getUserIndividual(): ?UserIndividual
    {
        return $this->user_individual;
    }

    public function setUserIndividual(?UserIndividual $user_individual): self
    {
        $this->user_individual = $user_individual;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserLegal()
    {
        return $this->user_legal;
    }

    /**
     * @param mixed $user_legal
     */
    public function setUserLegal($user_legal): void
    {
        $this->user_legal = $user_legal;
    }
}
